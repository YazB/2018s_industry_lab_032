package ictgradschool.industry.controlflow.guessing;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    private void start() {
        //Figure random number
        int computerGuess = getRandomNumber(1, 100);


        // loop:

        //     Ask user for number
        while(true) {
            //     While curly bracket closes at the bottom; is around the if else if
            int playerGuess = promptUserForNumber();
            //     Tell user if:
            //     Number correct, Number higher, Number lower
            //     Perfect; Goodbye

            if (playerGuess > computerGuess) {
                System.out.println("Too high, try again");
                // one condition of of loop
            } else if (playerGuess < computerGuess) {
                System.out.println("Too low, try again");
                //  second condition of loop
            } else {
                System.out.println("Perfect!");
                // If not condition a or b, go to else, where condition c lies
                break;
                // breaking free of loop; can continue on with code
            }
        }
        System.out.println("Goodbye");
        //      Outside of loop
    }

    private int getRandomNumber(int min, int range) {
        return (int) (Math.random() * (range) + min);
    }

    public int promptUserForNumber() {
        System.out.print("Enter your guess 1-100:");
        String a = Keyboard.readInput();
        //  Translating input so computer can process
        return Integer.parseInt(a);
        //  Translates number as integer and returns to user
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();


    }
}